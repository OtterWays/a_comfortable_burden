namespace otter_ways.cm
{
	public static class Layers
	{
		public const int PLAYER = 0b1;
		public const int STATIC = 0b10;
		public const int TASK = 0b100;
		public const int TRIGGER = 0b1000;
	}
}
