using Godot;

namespace otter_ways.cm.actors.environment
{
	public class WallTrigger : Node
	{
		[Export] systems.Player _Player;
		[Export] bool _StartingEnabled;
		[Export] Godot.Collections.Array<NodePath> _ToHidePaths;
		[Export] Godot.Collections.Array<NodePath> _ToFadePaths;

		Actor[] _ToHide;
		Actor[] _ToFade;

		bool _AlreadyTriggered;
		bool _ShouldTriggerOnControllerChanged;

		public override void 
		_EnterTree()
		{
			_ToHide = new Actor[_ToHidePaths.Count];
			_ToFade = new Actor[_ToFadePaths.Count];

			for (int i = 0; i < _ToHidePaths.Count; i++)
			{
				_ToHide[i] = GetNode<Actor>(_ToHidePaths[i]);
			}
			for (int i = 0; i < _ToFadePaths.Count; i++)
			{
				_ToFade[i] = GetNode<Actor>(_ToFadePaths[i]);
			}

			_ = _Player.Connect(nameof(systems.Player.ControllerChanged), this, nameof(OnPlayerControllerChanged));
		}

		public override void
		_ExitTree()
		{
			_Player.Disconnect(nameof(systems.Player.ControllerChanged), this, nameof(OnPlayerControllerChanged));
		}

		void
		OnPlayerControllerChanged()
		{
			if (_ShouldTriggerOnControllerChanged)
			{
				_ShouldTriggerOnControllerChanged = false;
				HideAndFadeActors();
			}
		}

		void
		OnBodyEntered(object body)
		{
			playable.Controller controller = body as playable.Controller;
			if (controller != null)
			{
				if (!_AlreadyTriggered)
				{
					_AlreadyTriggered = true;
					if (_StartingEnabled)
					{
						_StartingEnabled = false;
						return;
					}
					HideAndFadeActors();
				}
				else
				{
					_ShouldTriggerOnControllerChanged = true;
				}
			}
		}

		void
		OnBodyExited(object body)
		{
			playable.Controller controller = body as playable.Controller;
			if (controller != null)
			{
				_AlreadyTriggered = false;
				ResetActors();
			}
		}

		void
		HideAndFadeActors()
		{
			for (int i = 0; i < _ToHide.Length; i++)
			{
				_ToHide[i].Disappear();
			}
			for (int i = 0; i < _ToFade.Length; i++)
			{
				_ToFade[i].ReduceAlpha();
			}
		}

		void
		ResetActors()
		{
			for (int i = 0; i < _ToHide.Length; i++)
			{
				_ToHide[i].ResetSprite();
			}
			for (int i = 0; i < _ToFade.Length; i++)
			{
				_ToFade[i].ResetAlpha();
			}
		}
	}
}
