using Godot;

namespace otter_ways.cm.actors.items.task
{
	public class Task : Actor, thing
	{
		[Signal] public delegate void Completed(int id);
		[Signal] public delegate void Revealed(int id);

		[Export] systems.TaskList _TaskList;
		//[Export] systems.MentalLoad _MentalLoad;
		[Export] Texture _CleanFrame;
		[Export] NodePath _RigidBodyPath;
		[Export] NodePath _OutlinePath;
		[Export] NodePath _FXPath;
		[Export] readonly Type _TYPE;
		[Export] readonly float _DURATION = 2f;
		//[Export] readonly float _COST = 10f;
		[Export] readonly string _TEXT_TO_DISPLAY;
		[Export] readonly bool _START_REVEALED;
		[Export] readonly bool _START_HIDDEN;
		[Export] readonly bool _NO_COLLISIONS;
		[Export] readonly bool _CORRECT_OFFSET;
		[Export] Vector2 _NewOffset;

		public string TextToDisplay { get { return _TEXT_TO_DISPLAY; } }
		public Type Type { get { return _TYPE; } }
		public bool StartRevealed { get { return _START_REVEALED; } }
		public bool CanBeDone { get { return !_Completed; } }

		FX _FX;
		Outline _Outline;
		RigidBody _RigidBody;
		float _CompletionCurrentTime;
		int _ID;
		bool _Completed;

		public override void 
		_EnterTree()
		{
			_RigidBody = GetNode<RigidBody>(_RigidBodyPath);
			_Outline = GetNode<Outline>(_OutlinePath);
			_FX = GetNode<FX>(_FXPath);
			_ID = _TaskList.Register(this);
			base._EnterTree();
			if (_START_HIDDEN)
			{
				Disappear();
			}
		}

		public override void 
		_Ready()
		{
			_RigidBody.Initialise(this, _NO_COLLISIONS);
			_Outline.Initialise(_START_REVEALED);
			SetPhysicsProcess(false);
		}

		public override void 
		_PhysicsProcess(float delta)
		{
			_CompletionCurrentTime += delta;
			if (_CompletionCurrentTime >= _DURATION)
			{
				SetPhysicsProcess(false);
				Complete();
			}
		}

		void
		OnOutlineRevealed()
		{
			//_MentalLoad.IncreaseLoad(_MentalLoad.DiscoveryCost);

			EmitSignal(nameof(Revealed), _ID);
		}

		public void
		StartCompletion()
		{
			SetPhysicsProcess(true);
			//_MentalLoad.IncreaseLoad(_MentalLoad.ExecutionCost);
			_Outline.Hide();
			_FX.Start();
		}

		void
		Complete()
		{
			_Completed = true;

			_Sprite.Texture = _CleanFrame;
			if (_CORRECT_OFFSET)
			{
				_Sprite.Offset = _NewOffset;
			}
			_FX.Stop();

			EmitSignal(nameof(Completed), _ID);
		}

		public override void 
		Disappear()
		{
			base.Disappear();
			if (!_StayHidden && !_NO_COLLISIONS)
			{
				_RigidBody.DisableCollisions();
			}
		}

		public override void 
		ResetSprite()
		{
			if (_StayHidden)
			{
				_StayHidden = false;
			}
			else
			{
				if (!_NO_COLLISIONS)
				{
					_RigidBody.EnableCollisions();
				}
				Show();
				_Hidden = false;
			}
		}

		public override void 
		ReduceAlpha()
		{
			base.ReduceAlpha();
			if (!_StayTransparent)
			{
				if (!_NO_COLLISIONS)
				{
					_RigidBody.DisableCollisions();
				}
				_Outline.Hide();
			}
		}

		public override void 
		ResetAlpha()
		{
			if (_StayTransparent)
			{
				_StayTransparent = false;
			}
			else
			{
				_Transparent = false;
				_Sprite.Modulate = new Color(_Sprite.Modulate.r, _Sprite.Modulate.g,
											 _Sprite.Modulate.b, _DefaultAlpha);
				if (!_NO_COLLISIONS)
				{
					_RigidBody.EnableCollisions();
				}
				if (CanBeDone)
				{
					_Outline.Show();
				}
			}
		}
	}
}
