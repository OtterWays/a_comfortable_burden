﻿namespace otter_ways.cm.actors.items.task
{
    public enum Type
    {
        Watering,
        Cleaning,
        Mop
    };
}