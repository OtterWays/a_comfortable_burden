using Godot;

namespace otter_ways.cm.actors.items.task
{
	public class Outline : Sprite
	{
		[Signal] public delegate void Revealed();

		[Export] Settings _Settings;
		[Export] NodePath _TriggerAreaPath;
		[Export] readonly float _DETECTION_DEADZONE;
		[Export] readonly float _DETECTION_TRESHOLD;

		Area2D _TriggerArea;
		playable.Controller _Player;
		float _DetectionRange;
		float _PlayerDistance;
		float _CurrentAlpha;
		float _LastAlpha;
		bool _Revealed;

		public override void
		_EnterTree()
		{
			_TriggerArea = GetNode<Area2D>(_TriggerAreaPath);

			_DetectionRange = ((CircleShape2D)_TriggerArea.ShapeOwnerGetShape(0, 0)).Radius;

			Modulate = new Color(_Settings.OutlineColour.r, _Settings.OutlineColour.g, _Settings.OutlineColour.b, 0f);

			_ = _Settings.Connect(nameof(Settings.ColourUpdated), this, nameof(OnSettingsColourUpdated));
		}

		public override void
		_ExitTree()
		{
			_Settings.Disconnect(nameof(Settings.ColourUpdated), this, nameof(OnSettingsColourUpdated));
		}

		public override void
		_Ready()
		{
			SetProcess(false);
		}

		public override void
		_Process(float delta)
		{
			_PlayerDistance = (GlobalPosition + _TriggerArea.Position - _Player.GlobalPosition).Length();

			_CurrentAlpha = 1f - ((_PlayerDistance - _DETECTION_DEADZONE) /
										 (_DetectionRange - _DETECTION_DEADZONE));
			_CurrentAlpha = _CurrentAlpha > 0f ? _CurrentAlpha : 0f;
			_CurrentAlpha = _CurrentAlpha > _LastAlpha ? _CurrentAlpha : _LastAlpha;
			_LastAlpha = _CurrentAlpha;

			Modulate = new Color(_Settings.OutlineColour.r, _Settings.OutlineColour.g,
								 _Settings.OutlineColour.b, _CurrentAlpha);

			if (_CurrentAlpha > _DETECTION_TRESHOLD)
			{
				SetProcess(false);
				Reveal();
			}
		}

		void
		OnAreaBodyEntered(object body)
		{
			if (!_Revealed)
			{
				playable.Controller controller = body as playable.Controller;
				if (controller != null)
				{
					_Player = controller;
					SetProcess(true);
				}
			}
		}

		void
		OnAreaBodyExited(object body)
		{
			if (!_Revealed)
			{
				playable.Controller controller = body as playable.Controller;
				if (controller != null)
				{
					SetProcess(false);
				}
			}
		}

		void
		OnSettingsColourUpdated()
		{
			Modulate = new Color(_Settings.OutlineColour.r, _Settings.OutlineColour.g,
								 _Settings.OutlineColour.b, Modulate.a);
		}

		public void
		Initialise(bool startrevealed)
		{
			if (startrevealed)
			{
				_Revealed = true;
				Modulate = new Color(_Settings.OutlineColour.r, _Settings.OutlineColour.g,
									 _Settings.OutlineColour.b, 1f);
			}
		}

		void
		Reveal()
		{
			_Revealed = true;

			_Player = null;
			Modulate = new Color(_Settings.OutlineColour.r, _Settings.OutlineColour.g, _Settings.OutlineColour.b, 1f);

			EmitSignal(nameof(Revealed));
		}
	}
}
