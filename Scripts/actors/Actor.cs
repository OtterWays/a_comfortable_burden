using Godot;

namespace otter_ways.cm.actors
{
	public abstract class Actor : Node2D
	{
		[Export] NodePath _SpritePath;
		[Export(PropertyHint.Range, "0,255,1")] protected readonly float _REDUCED_ALPHA;
		[Export] readonly bool _STARTING_TRANSPARENT;

		protected Sprite _Sprite;
		protected float _DefaultAlpha { get; private set; }
		protected bool _Hidden;
		protected bool _StayHidden;
		protected bool _Transparent;
		protected bool _StayTransparent;

		public override void
		_EnterTree()
		{
			_Sprite = GetNode<Sprite>(_SpritePath);
			_DefaultAlpha = _Sprite.Modulate.a;
			if (_STARTING_TRANSPARENT)
			{
				ReduceAlpha();
			}
		}

		public virtual void 
		Disappear()
		{
			_StayHidden = _Hidden ? true : false;
			_Hidden = true;
			if (!_StayHidden)
			{
				Hide();
			}
		}

		public virtual void
		ResetSprite()
		{
			if (_StayHidden)
			{
				_StayHidden = false;
			}
			else
			{
				Show();
				_Hidden = false;
			}
		}

		public virtual void
		ReduceAlpha()
		{
			_StayTransparent = _Transparent ? true : false;
			_Transparent = true;
			if (!_StayTransparent)
			{
				_Sprite.Modulate = new Color(_Sprite.Modulate.r, _Sprite.Modulate.g, 
											 _Sprite.Modulate.b, _REDUCED_ALPHA / 255f);
			}
		}

		public virtual void
		ResetAlpha()
		{
			if (_StayTransparent)
			{
				_StayTransparent = false;
			}
			else
			{
				_Transparent = false;
				_Sprite.Modulate = new Color(_Sprite.Modulate.r, _Sprite.Modulate.g, 
											 _Sprite.Modulate.b, _DefaultAlpha);
			}
		}
	}
}
