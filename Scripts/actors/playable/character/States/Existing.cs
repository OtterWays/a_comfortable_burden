namespace otter_ways.cm.actors.playable.character.states
{
    public enum Existing
    {
        Disabled,
        Waiting,
        Idle,
        Movement,
        Action
    };
}