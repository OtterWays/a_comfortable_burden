namespace otter_ways.cm.actors.playable.character.states
{
	//>(Blanche) TODO: Audio
    public class Movement : State
	{
		public override Existing Type { get { return Existing.Movement; } }

        const float _AR_THRESHOLD = 0.01f;

		Godot.Vector2 _InputVector;
		Godot.Vector2 _InputVectorRotated;
		float _InputLengthLastUpdate;

		float _ARCurrentTime;
		bool _Attacking;
		bool _Releasing;
		bool _InputVectorNotZero;

		Godot.Vector2 _FinalVelocity;

		public
        Movement(Controller controller) : base(controller)
        {
			_FinalVelocity = new Godot.Vector2();
			_InputVector = new Godot.Vector2();
			_InputVectorRotated = new Godot.Vector2();
		}

        public override void
        Enter(Existing previousstate)
        { }

        public override void
        PhysicsProcess(float delta, Godot.Physics2DDirectSpaceState spacestate)
		{
			if (!_InputVectorNotZero && _Controller._Velocity.Length() < 0.2f)
			{
				_Controller.MovementToIdle();
				_Controller.ForceStatePhysics(delta, spacestate);
				return;
			}

            if (_InputVector.Length() > _AR_THRESHOLD && _InputLengthLastUpdate < _AR_THRESHOLD)
            {
                _ARCurrentTime = 0f;
                _Releasing = false;
                _Attacking = true;
            }
            else if (_InputVector.Length() < _AR_THRESHOLD && _InputLengthLastUpdate > _AR_THRESHOLD)
            {
                _ARCurrentTime = 0f;
                _Attacking = false;
                _Releasing = true;
                _FinalVelocity = _Controller._Velocity;
            }

			_InputVectorRotated = _InputVector;
			if (_InputVectorRotated.x != 0f)
            {
				_InputVectorRotated.y = _InputVectorRotated.x * .5f;
				if (_InputVectorRotated.x < 0f && (_Controller.FacingDirection != Controller.LEFT || _InputLengthLastUpdate == 0f))
				{
					_Controller.Animator.PlayRunLeft();
					_Controller.FacingDirection = Controller.LEFT;
                }
				else if (_InputVectorRotated.x > 0f && (_Controller.FacingDirection != Controller.RIGHT || _InputLengthLastUpdate == 0f))
                {
					_Controller.Animator.PlayRunRight();
					_Controller.FacingDirection = Controller.RIGHT;
                }
            }
			else if (_InputVectorRotated.y != 0f)
            {
				_InputVectorRotated.x = _InputVectorRotated.y * -2f;
				if (_InputVectorRotated.y < 0f && (_Controller.FacingDirection != Controller.UP || _InputLengthLastUpdate == 0f))
				{
					_Controller.Animator.PlayRunUp();
					_Controller.FacingDirection = Controller.UP;
				}
				else if(_InputVectorRotated.y > 0f && (_Controller.FacingDirection != Controller.DOWN || _InputLengthLastUpdate == 0f))
				{
					_Controller.Animator.PlayRunDown();
					_Controller.FacingDirection = Controller.DOWN;
				}
			}
			_Controller._Velocity = _InputVectorRotated.Normalized() * _Controller.Movement.Speed;

			if (_Attacking)
			{
				_ARCurrentTime += delta;
				if (_ARCurrentTime >= _Controller.Movement.AttackTime)
				{
					_Controller._Velocity *= _Controller.Movement.Attack.Curve.Interpolate(1f);
					_Attacking = false;
					_ARCurrentTime = 0f;
				}
				else
				{
					_Controller._Velocity *= _Controller.Movement.Attack.Curve.Interpolate(_ARCurrentTime / _Controller.Movement.AttackTime);
				}
			}
			else if (_Releasing)
			{
				_ARCurrentTime += delta;
				_Controller._Velocity = _FinalVelocity;
				if (_ARCurrentTime >= _Controller.Movement.ReleaseTime)
				{
					_Controller._Velocity *= _Controller.Movement.Release.Curve.Interpolate(1f);
					_Releasing = false;
					_ARCurrentTime = 0f;
				}
				else
				{
					_Controller._Velocity *= _Controller.Movement.Release.Curve.Interpolate(_ARCurrentTime / _Controller.Movement.ReleaseTime);
				}
			}

			_Controller._Velocity = _Controller.MoveAndSlide(_Controller._Velocity);

			//x!(Blanche) UGH
			//_Controller.MentalLoad.IncreaseLoad(_Controller.MentalLoad.MovementCost);

			_InputLengthLastUpdate = _InputVector.Length();
		}

		public override void
		Exit(Existing nextstate)
		{
			_InputLengthLastUpdate = 0f;
			_InputVectorNotZero = false;
			_Attacking = false;
			_Releasing = false;
			_ARCurrentTime = 0f;
			_InputVector = Godot.Vector2.Zero;
			_FinalVelocity = Godot.Vector2.Zero;
		}

		public override void
        Move(float horizontal, float vertical)
		{
			_InputVector.x = horizontal;
			_InputVector.y = vertical;
			_InputVectorNotZero = _InputVector.LengthSquared() > 0f;
		}

		public override void
        DoSomething()
        {
            _Controller.PushAction();
		}
	}
}
