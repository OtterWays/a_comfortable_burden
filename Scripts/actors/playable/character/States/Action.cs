namespace otter_ways.cm.actors.playable.character.states
{
    public class Action : State
    {
		public override Existing Type { get { return Existing.Action; } }

        readonly Godot.Vector2 _RAYCAST_START = new Godot.Vector2(8f, 4f);
        readonly Godot.Vector2 _RAYCAST_LENGTH = new Godot.Vector2(48, 24);
        Godot.Collections.Dictionary _RayCastResults;
        items.task.thing _TaskHit;

        bool _ShouldRayCast;

        bool _isDoingSomething;

        int _SoundCount;
     
        public
        Action(Controller controller) : base(controller)
        { }

        public override void
        Enter(Existing previousstate)
        {
            switch(_TaskHit.Type)
            {
                case items.task.Type.Cleaning:
                    if (_Controller.FacingDirection == Controller.UP)
                    {
                        _Controller.Animator.PlayCleanUp();
                    }
                    else if (_Controller.FacingDirection == Controller.RIGHT)
                    {
                        _Controller.Animator.PlayCleanRight();
                    }
                    else if (_Controller.FacingDirection == Controller.DOWN)
                    {
                        _Controller.Animator.PlayCleanDown();
                    }
                    else if (_Controller.FacingDirection == Controller.LEFT)
                    {
                        _Controller.Animator.PlayCleanLeft();
                    }
                    _Controller.AudioPlayer.Play(_Controller.Sounds.CLEANING_SOUNDS[0], 1.25f);
                    break;
                case items.task.Type.Mop:
                    if (_Controller.FacingDirection == Controller.UP)
                    {
                        _Controller.Animator.PlayMopUp();
                    }
                    else if (_Controller.FacingDirection == Controller.RIGHT)
                    {
                        _Controller.Animator.PlayMopRight();
                    }
                    else if (_Controller.FacingDirection == Controller.DOWN)
                    {
                        _Controller.Animator.PlayMopDown();
                    }
                    else if (_Controller.FacingDirection == Controller.LEFT)
                    {
                        _Controller.Animator.PlayMopLeft();
                    }
                    _Controller.AudioPlayer.Play(_Controller.Sounds.MOPING_SOUNDS[_SoundCount % 2], 1.25f);
                    break;
                case items.task.Type.Watering:
                    if (_Controller.FacingDirection == Controller.UP)
                    {
                        _Controller.Animator.PlayWaterUp();
                    }
                    else if (_Controller.FacingDirection == Controller.RIGHT)
                    {
                        _Controller.Animator.PlayWaterRight();
                    }
                    else if (_Controller.FacingDirection == Controller.DOWN)
                    {
                        _Controller.Animator.PlayWaterDown();
                    }
                    else if (_Controller.FacingDirection == Controller.LEFT)
                    {
                        _Controller.Animator.PlayWaterLeft();
                    }
                    _Controller.AudioPlayer.Play(_Controller.Sounds.WATERING_SOUNDS[_SoundCount % 2], 1.25f);
                    break;
            }
            _SoundCount++;
        }

        public override void
        PhysicsProcess(float delta, Godot.Physics2DDirectSpaceState spacestate)
        {
            if (_ShouldRayCast)
            {
                _RayCastResults = 
                    spacestate.IntersectRay(_Controller.Position,
                                            _Controller.Position + (_RAYCAST_LENGTH * _Controller.FacingDirection),
                                            null, cm.Layers.TASK);
                if (_RayCastResults.Count > 0)
                {
                    _TaskHit = (_RayCastResults["collider"] as items.task.RigidBody).Parent;
                    if (_TaskHit != null)
                    {
                        ProcessTaskFound();
                    }
                }

                if (_TaskHit == null)
                {
                    bool TaskFound = false;

                    if (_Controller.FacingDirection == Controller.UP || 
                        _Controller.FacingDirection == Controller.DOWN)
                    {
                        _RayCastResults = 
                            spacestate.IntersectRay(_Controller.Position + (_RAYCAST_START * Controller.LEFT),
                                                    _Controller.Position + (_RAYCAST_START * Controller.LEFT) + (_RAYCAST_LENGTH * _Controller.FacingDirection),
                                                    null, cm.Layers.TASK);
                        if (_RayCastResults.Count > 0)
                        {
                            _TaskHit = (_RayCastResults["collider"] as items.task.RigidBody).Parent;
                            if (_TaskHit != null)
                            {
                                TaskFound = true;
                            }
                        }

                        _RayCastResults = 
                            spacestate.IntersectRay(_Controller.Position + (_RAYCAST_START * Controller.RIGHT),
                                                    _Controller.Position + (_RAYCAST_START * Controller.RIGHT) + (_RAYCAST_LENGTH * _Controller.FacingDirection),
                                                    null, cm.Layers.TASK);
                        if (_RayCastResults.Count > 0)
                        {
                            _TaskHit = (_RayCastResults["collider"] as items.task.RigidBody).Parent;
                            if (_TaskHit != null)
                            {
                                TaskFound = true;
                            }
                        }
                    }
                    else if (_Controller.FacingDirection == Controller.RIGHT || 
                             _Controller.FacingDirection == Controller.LEFT)
                    {
                        _RayCastResults = 
                            spacestate.IntersectRay(_Controller.Position + (_RAYCAST_START * Controller.UP),
                                                    _Controller.Position + (_RAYCAST_START * Controller.UP) + (_RAYCAST_LENGTH * _Controller.FacingDirection),
                                                    null, cm.Layers.TASK);
                        if (_RayCastResults.Count > 0)
                        {
                            _TaskHit = (_RayCastResults["collider"] as items.task.RigidBody).Parent;
                            if (_TaskHit != null)
                            {
                                TaskFound = true;
                            }
                        }

                        _RayCastResults = 
                            spacestate.IntersectRay(_Controller.Position + (_RAYCAST_START * Controller.DOWN),
                                                    _Controller.Position + (_RAYCAST_START * Controller.DOWN) + (_RAYCAST_LENGTH * _Controller.FacingDirection),
                                                    null, cm.Layers.TASK);
                        if (_RayCastResults.Count > 0)
                        {
                            _TaskHit = (_RayCastResults["collider"] as items.task.RigidBody).Parent;
                            if (_TaskHit != null)
                            {
                                TaskFound = true;
                            }
                        }
                    }

                    if (TaskFound)
                    {
                        ProcessTaskFound();
                    }
                }

                _RayCastResults = null;
                _ShouldRayCast = false;

                _Controller.ActionToAction(_isDoingSomething);
                _Controller.ForceStatePhysics(delta, spacestate);
                return;
            }
        }

        public override void 
        Exit(Existing nextstate)
        {
            _TaskHit.Disconnect(nameof(items.task.Task.Completed), _Controller, nameof(Controller.OnTaskCompleted));
            _TaskHit = null;
            _isDoingSomething = false;
        }

        public override void
        DoSomething()
        {
            if(!_isDoingSomething)
            {
                _ShouldRayCast = true;
            }
        }

        void
        ProcessTaskFound()
        {
            _isDoingSomething = _TaskHit.CanBeDone;
            if (_TaskHit.CanBeDone)
            {
                _TaskHit.Connect(nameof(items.task.Task.Completed), _Controller, nameof(Controller.OnTaskCompleted));
                _TaskHit.StartCompletion();
            }
        }
    }
}
