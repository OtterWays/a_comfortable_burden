using Godot;

namespace otter_ways.cm.actors.playable.character
{
    public class AnimatorHandler : AnimationPlayer
    {
        const string _RUN_RIGHT = "run_right";
        const string _RUN_UP = "run_up";
        const string _IDLE_RIGHT = "idle_right";
        const string _IDLE_UP = "idle_up";
        const string _CLEAN_RIGHT = "clean_right";
        const string _CLEAN_UP = "clean_up";
        const string _MOP_RIGHT = "mop_right";
        const string _MOP_UP = "mop_up";
        const string _WATER_RIGHT = "water_right";
        const string _WATER_UP = "water_up";

        [Export] NodePath _SpritePath;

        public bool FlipH
        {
            get { return _Sprite.FlipH; }
            set { _Sprite.FlipH = value; }
        }

        Sprite _Sprite;

        public override void 
        _EnterTree()
        {
            _Sprite = GetNode<Sprite>(_SpritePath);
        }

        public void
        StopEverything()
        {
            if (IsPlaying())
            {
                Stop(true);
            }
        }

        public void
        PlayRunRight()
        {
            _Sprite.FlipH = false;
            //if (!IsPlaying() || CurrentAnimation == _RUN_UP)
            //{
                Play(_RUN_RIGHT);
            //}
        }

        public void
        PlayRunLeft()
        {
            _Sprite.FlipH = true;
            //if (!IsPlaying() || CurrentAnimation == _RUN_RIGHT)
            //{
                Play(_RUN_UP);
            //}
        }

        public void
        PlayRunUp()
        {
            _Sprite.FlipH = false;
            //if (!IsPlaying() || CurrentAnimation == _RUN_RIGHT)
            //{
                Play(_RUN_UP);
            //}
        }

        public void
        PlayRunDown()
        {
            _Sprite.FlipH = true;
            //if (!IsPlaying() || CurrentAnimation == _RUN_UP)
            //{
                Play(_RUN_RIGHT);
            //}
        }

        public void
        PlayIdleRight()
        {
            _Sprite.FlipH = false;
            //if (!IsPlaying() || CurrentAnimation == _RUN_UP)
            //{
                Play(_IDLE_RIGHT);
            //}
        }

        public void
        PlayIdleLeft()
        {
            _Sprite.FlipH = true;
            //if (!IsPlaying() || CurrentAnimation == _RUN_RIGHT)
            //{
                Play(_IDLE_UP);
            //}
        }

        public void
        PlayIdleUp()
        {
            _Sprite.FlipH = false;
            //if (!IsPlaying() || CurrentAnimation == _RUN_RIGHT)
            //{
                Play(_IDLE_UP);
            //}
        }

        public void
        PlayIdleDown()
        {
            _Sprite.FlipH = true;
            //if (!IsPlaying() || CurrentAnimation == _RUN_UP)
            //{
                Play(_IDLE_RIGHT);
            //}
        }

        public void
        PlayCleanRight()
        {
            _Sprite.FlipH = false;
            //if (!IsPlaying() || CurrentAnimation == _RUN_UP)
            //{
            Play(_CLEAN_RIGHT);
            //}
        }

        public void
        PlayCleanLeft()
        {
            _Sprite.FlipH = true;
            //if (!IsPlaying() || CurrentAnimation == _RUN_RIGHT)
            //{
            Play(_CLEAN_UP);
            //}
        }

        public void
        PlayCleanUp()
        {
            _Sprite.FlipH = false;
            //if (!IsPlaying() || CurrentAnimation == _RUN_RIGHT)
            //{
            Play(_CLEAN_UP);
            //}
        }

        public void
        PlayCleanDown()
        {
            _Sprite.FlipH = true;
            //if (!IsPlaying() || CurrentAnimation == _RUN_UP)
            //{
            Play(_CLEAN_RIGHT);
            //}
        }

        public void
        PlayMopRight()
        {
            _Sprite.FlipH = false;
            //if (!IsPlaying() || CurrentAnimation == _RUN_UP)
            //{
            Play(_MOP_RIGHT);
            //}
        }

        public void
        PlayMopLeft()
        {
            _Sprite.FlipH = true;
            //if (!IsPlaying() || CurrentAnimation == _RUN_RIGHT)
            //{
            Play(_MOP_UP);
            //}
        }

        public void
        PlayMopUp()
        {
            _Sprite.FlipH = false;
            //if (!IsPlaying() || CurrentAnimation == _RUN_RIGHT)
            //{
            Play(_MOP_UP);
            //}
        }

        public void
        PlayMopDown()
        {
            _Sprite.FlipH = true;
            //if (!IsPlaying() || CurrentAnimation == _RUN_UP)
            //{
            Play(_MOP_RIGHT);
            //}
        }

        public void
        PlayWaterRight()
        {
            _Sprite.FlipH = false;
            //if (!IsPlaying() || CurrentAnimation == _RUN_UP)
            //{
            Play(_WATER_RIGHT);
            //}
        }

        public void
        PlayWaterLeft()
        {
            _Sprite.FlipH = true;
            //if (!IsPlaying() || CurrentAnimation == _RUN_RIGHT)
            //{
            Play(_WATER_UP);
            //}
        }

        public void
        PlayWaterUp()
        {
            _Sprite.FlipH = false;
            //if (!IsPlaying() || CurrentAnimation == _RUN_RIGHT)
            //{
            Play(_WATER_UP);
            //}
        }

        public void
        PlayWaterDown()
        {
            _Sprite.FlipH = true;
            //if (!IsPlaying() || CurrentAnimation == _RUN_UP)
            //{
            Play(_WATER_RIGHT);
            //}
        }
    }
}
