using Godot;

namespace otter_ways.cm.ui
{
	public class TaskScreen : Control
	{
		[Signal] public delegate void GOPressed();

		[Export] systems.GameRules _GameRules;
		[Export] systems.TaskList _TaskList;
		[Export] NodePath _FirstButtonPath;
		[Export] NodePath _TasksContainerPath;
		[Export] PackedScene _TaskLabelScene;

		System.Collections.Generic.List<Label> _TasksLabels;
		Label _LabelInstance;
		Control _TasksContainer;
		Button _FirstButton;

		public override void
		_EnterTree()
		{
			_TasksLabels = new System.Collections.Generic.List<Label>();
			_TasksContainer = GetNode<Control>(_TasksContainerPath);
			_FirstButton = GetNode<Button>(_FirstButtonPath);
			_ = _TaskList.Connect(nameof(systems.TaskList.Readied), this, nameof(OnTaskListReadied));
		}

		public override void
		_ExitTree()
		{
			_TaskList.Disconnect(nameof(systems.TaskList.Readied), this, nameof(OnTaskListReadied));
		}

		void
		OnTaskListReadied()
		{
			if (_GameRules.CurrentGameMode == systems.GameMode.Man)
			{
				for (int i = 0; i < _TaskList.Tasks.Count; i++)
				{
					//if (_TaskList.Tasks[i].StartRevealed)
					//{
						_LabelInstance = (Label)_TaskLabelScene.Instance();
						_LabelInstance.Text = _TaskList.Tasks[i].TextToDisplay;
						_TasksLabels.Add(_LabelInstance);
						_TasksContainer.AddChild(_LabelInstance);
						_LabelInstance = null;
					//}
				}

				Show();
				_FirstButton.GrabFocus();
			}
		}

		void
		OnGOPressed()
		{
			Hide();

			for (int i = 0; i < _TasksLabels.Count; i++)
			{
				if (!_TasksLabels[i].IsQueuedForDeletion())
				{
					_TasksLabels[i].QueueFree();
				}
			}

			_TasksLabels.Clear();
			EmitSignal(nameof(GOPressed));
		}
	}
}
