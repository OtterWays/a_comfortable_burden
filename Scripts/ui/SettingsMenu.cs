using Godot;

namespace otter_ways.cm.ui
{
	public class SettingsMenu : Control
	{
		[Signal] public delegate void Closing();

		[Export] Settings _Settings;
		[Export] NodePath _MusicSliderPath;
		[Export] NodePath _EffectsSliderPath;
		[Export] NodePath _LanguagesMenuButtonPath;
		[Export] NodePath _OutlineColourPath;
		[Export] NodePath _FirstControlPath;

		HSlider _MusicSlider;
		HSlider _EffectsSlider;
		MenuButton _LanguagesMenuButton;
		PopupMenu _LanguagesPopupMenu;
		ColorPickerButton _OutlineColourButton;
		ColorPicker _OutlineColourPicker;
		Control _FirstControl;
		const string _UI_FRENCH = "UI_FRENCH";
		const string _UI_ENGLISH = "UI_ENGLISH";
		const string _INDEX_PRESSED = "index_pressed";
		const string _COLOR_CHANGED = "color_changed";
		const string _POPUP_HIDE = "popup_hide";
		int _CurrentLPMIndex;
		bool _CancelPressed;
		bool _LanguagesPopupMenuOpened;
		bool _OutlineColourPickerOpened;

		public override void
		_EnterTree()
		{
			_FirstControl = GetNode<Control>(_FirstControlPath);
			_MusicSlider = GetNode<HSlider>(_MusicSliderPath);
			_EffectsSlider = GetNode<HSlider>(_EffectsSliderPath);
			_OutlineColourButton = GetNode<ColorPickerButton>(_OutlineColourPath);
			_OutlineColourPicker = _OutlineColourButton.GetPicker();
			_OutlineColourPicker.PresetsVisible = false;
			_LanguagesMenuButton = GetNode<MenuButton>(_LanguagesMenuButtonPath);
			_LanguagesPopupMenu = _LanguagesMenuButton.GetPopup();
			_CurrentLPMIndex = 0;

			_ = _LanguagesPopupMenu.Connect(_INDEX_PRESSED, this, nameof(OnLanguagesPopupMenuIndexPressed));
			_ = _LanguagesPopupMenu.Connect(_POPUP_HIDE, this, nameof(OnLanguagesPopupMenuPopupHide));
			_ = _OutlineColourPicker.Connect(_COLOR_CHANGED, this, nameof(OnOutlineColourColorChanged));
		}

		public override void 
		_ExitTree()
		{
			_LanguagesPopupMenu.Disconnect(_INDEX_PRESSED, this, nameof(OnLanguagesPopupMenuIndexPressed));
			_LanguagesPopupMenu.Disconnect(_POPUP_HIDE, this, nameof(OnLanguagesPopupMenuPopupHide));
			_OutlineColourPicker.Disconnect(_COLOR_CHANGED, this, nameof(OnOutlineColourColorChanged));
		}

		public override void
		_Ready()
		{
			SetProcessInput(false);
		}

		public override void
		_Input(InputEvent @event)
		{
			if (Input.IsActionJustPressed(InputAxes.CANCEL))
			{
				if (!_OutlineColourPickerOpened && !_LanguagesPopupMenuOpened && !_CancelPressed)
				{
					OnBackPressed();
					GetTree().SetInputAsHandled();
				}
			}
		}

		void
		OnLanguagesMenuButtonPressed()
		{
			_LanguagesPopupMenuOpened = true;
		}

		void
		OnLanguagesPopupMenuIndexPressed(int index)
		{
			_CurrentLPMIndex = index;
			string newLanguage = _LanguagesPopupMenu.GetItemText(_CurrentLPMIndex);
			_LanguagesMenuButton.Text = newLanguage;
			switch (newLanguage)
			{
				case _UI_FRENCH:
					newLanguage = "fr";
					break;
				case _UI_ENGLISH:
					newLanguage = "en";
					break;
			}
			_Settings.UpdateLanguage(newLanguage);
		}

		void
		OnLanguagesPopupMenuPopupHide()
		{
			_LanguagesPopupMenuOpened = false;
		}

		void
		OnMusicSliderValueChanged(float value)
		{
			_Settings.UpdateMusicVolume(value);
		}

		void
		OnEffectsSliderValueChanged(float value)
		{
			_Settings.UpdateEffectsVolume(value);
		}

		void
		OnOutlineColourButtonPressed()
		{
			_OutlineColourPickerOpened = true;
		}

		void
		OnOutlineColourButtonPopupClosed()
		{
			_OutlineColourPickerOpened = false;
		}

		void
		OnOutlineColourColorChanged(Color color)
		{
			_Settings.UpdateOutlineColour(color);
		}

		void
		OnBackPressed()
		{
			_CancelPressed = true;
			Hide();
			SetProcessInput(false);
			EmitSignal(nameof(Closing));
		}

		void
		OnUIRequestSettings()
		{
			_MusicSlider.Value = _Settings.Music;
			_EffectsSlider.Value = _Settings.Effects;
			
			string currentLocale = TranslationServer.GetLocale().Substring(0, 2).ToLower();
			
			switch(currentLocale)
			{
				case "fr":
					_LanguagesMenuButton.Text = _UI_FRENCH;
					break;
				case "en":
					_LanguagesMenuButton.Text = _UI_ENGLISH;
					break;
			}
			_OutlineColourButton.Color = _Settings.OutlineColour;

			_CancelPressed = false;
			Show();
			SetProcessInput(true);
			_FirstControl.GrabFocus();
		}
	}
}
