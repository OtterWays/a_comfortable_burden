using Godot;

namespace otter_ways.cm.ui
{
	public class CreditsMenu : Control
	{
		[Signal] public delegate void Closing();

		[Export] NodePath _FirstButtonPath;

		Button _FirstButton;
		bool _CancelPressed;

		public override void
		_EnterTree()
		{
			_FirstButton = GetNode<Button>(_FirstButtonPath);
		}

		public override void 
		_Ready()
		{
			SetProcessInput(false);
		}

		public override void
		_Input(InputEvent @event)
		{
			if (Input.IsActionJustPressed(InputAxes.CANCEL))
			{
				if (!_CancelPressed)
				{
					OnBackPressed();
					GetTree().SetInputAsHandled();
				}
			}
		}

		void
		OnBackPressed()
		{
			_CancelPressed = true;
			Hide();
			SetProcessInput(false);
			EmitSignal(nameof(Closing));
		}

		void
		OnUIRequestCredits()
		{
			_CancelPressed = false;
			Show();
			SetProcessInput(true);
			_FirstButton.GrabFocus();
		}
	}
}
