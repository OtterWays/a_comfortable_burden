using Godot;

namespace otter_ways.cm.ui
{
	public class PauseMenu : Control
	{
		[Signal] public delegate void RequestResume();
		[Signal] public delegate void RequestSettings();
		[Signal] public delegate void RequestTitleScreen();

		//TODO(Blanche): Check _FirstButtonPath has been set in the inspector
		[Export] NodePath _FirstButtonPath;

		Button _FirstButton;

		public override void
		_EnterTree()
		{
			_FirstButton = GetNode<Button>(_FirstButtonPath);
		}

		void
		OnResumePressed()
		{
			Close();
			EmitSignal(nameof(RequestResume));
		}

		void
		OnSettingsPressed()
		{
			Hide();
			EmitSignal(nameof(RequestSettings));
		}

		void
		OnTitleScreenPressed()
		{
			EmitSignal(nameof(RequestTitleScreen));
		}

		void
		OnSettingsMenuClosing()
		{
			Show();
			_FirstButton.GrabFocus();
		}

		public void
		Open()
		{
			Show();
			_FirstButton.GrabFocus();
		}

		public void
		Close()
		{
			Hide();
		}
	}
}
