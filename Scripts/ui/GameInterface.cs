using Godot;

namespace otter_ways.cm.ui
{
	public class GameInterface : Control
	{
		[Export] systems.GameRules _GameRules;

		public override void 
		_EnterTree()
		{
			_ = _GameRules.Connect(nameof(systems.GameRules.LevelStarting), this, nameof(OnGameRulesLevelStarting));
			_ = _GameRules.Connect(nameof(systems.GameRules.Victory), this, nameof(OnGameRulesVictory));
			_ = _GameRules.Connect(nameof(systems.GameRules.Defeat), this, nameof(OnGameRulesDefeat));
		}

		public override void 
		_ExitTree()
		{
			_GameRules.Disconnect(nameof(systems.GameRules.LevelStarting), this, nameof(OnGameRulesLevelStarting));
			_GameRules.Disconnect(nameof(systems.GameRules.Victory), this, nameof(OnGameRulesVictory));
			_GameRules.Disconnect(nameof(systems.GameRules.Defeat), this, nameof(OnGameRulesDefeat));
		}

		void
		OnGameRulesLevelStarting()
		{
			Show();
		}

		void
		OnGameRulesVictory()
		{
			Hide();
		}

		void
		OnGameRulesDefeat()
		{
			Hide();
		}
	}
}
