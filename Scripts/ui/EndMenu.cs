using Godot;

namespace otter_ways.cm.ui
{
	public class EndMenu : Control
	{
		[Signal] public delegate void RequestTitleScreen();

		[Export] systems.GameRules _GameRules;
		//>(Blanche) TODO: Check _FirstButtonPath has been set in the inspector
		[Export] NodePath _FirstButtonPath;
		[Export] NodePath _MenuContainerPath;

		Button _FirstButton;
		Control _MenuContainer;

		public override void
		_EnterTree()
		{
			_FirstButton = GetNode<Button>(_FirstButtonPath);
			_MenuContainer = GetNode<Control>(_MenuContainerPath);
			_ = _GameRules.Connect(nameof(systems.GameRules.GameEnd), this, nameof(OnGameRulesGameEnd));
		}

		public override void
		_ExitTree()
		{
			_GameRules.Disconnect(nameof(systems.GameRules.GameEnd), this, nameof(OnGameRulesGameEnd));
		}

		void
		OnGameRulesGameEnd()
		{
			Show();
			_FirstButton.GrabFocus();
		}

		void
		OnResourcesPressed()
		{
			_MenuContainer.Hide();
		}

		void
		OnCreditsPressed()
		{
			_MenuContainer.Hide();
		}

		void
		OnTitleScreenPressed()
		{
			//>(Blanche) NOTE: Every scenes opened will be unloaded then the title screen scene will be loaded, it's useless to clean before
			EmitSignal(nameof(RequestTitleScreen));
		}

		void
		OnSubMenuClosing()
		{
			_MenuContainer.Show();
			_FirstButton.GrabFocus();
		}
	}
}
