using Godot;

namespace otter_ways.cm
{
    public class LevelConfiguration : Resource
    {
        [Export] systems.GameMode _GameMode;
        public systems.GameMode GameMode { get { return _GameMode; } }

        [Export] float _TimerDuration = 30f;
        public float TimerDuration { get { return _TimerDuration; } }
        
        [Export] PackedScene _Scene;
        public PackedScene Scene { get { return _Scene; } }
    }
}
