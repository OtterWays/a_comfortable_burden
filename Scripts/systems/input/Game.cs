using Godot;

namespace otter_ways.cm.systems.input
{
    public class Game : State
	{
		float _ARCurrentTime;
		bool _Attacking;
		bool _Releasing;

		bool _ShouldUpdateMove;
		bool _LastUpdateMove;

		Vector2 _InpVecCur;
		Vector2 _InpVecOld;
		Vector2 _InputVectorGoal;
		Vector2 _InputVectorFinal;

		public
        Game(Player player) : base(player)
		{
			_InpVecCur = new Vector2();
			_InpVecOld = new Vector2();
			_InputVectorGoal = new Vector2();
			_InputVectorFinal = new Vector2();
		}

        public override void
        _UnhandledInput(InputEvent @event)
		{
			if (Input.IsActionJustPressed(InputAxes.PAUSE_MENU))
			{
				_Player.EmitPressedPause();
				_Player.ToPauseMenu();
				return;
			}

			if (Input.IsActionJustPressed(InputAxes.ACTION))
			{
				_Player.CurrentController.DoSomething();
			}

			_InpVecCur.x = (Input.GetActionStrength(InputAxes.DIGITAL_RIGHT) + 
							Input.GetActionStrength(InputAxes.ANALOG_RIGHT))
						 - (Input.GetActionStrength(InputAxes.DIGITAL_LEFT) + 
							Input.GetActionStrength(InputAxes.ANALOG_LEFT));
			_InpVecCur.y = (Input.GetActionStrength(InputAxes.DIGITAL_DOWN) + 
							Input.GetActionStrength(InputAxes.ANALOG_DOWN))
						 - (Input.GetActionStrength(InputAxes.DIGITAL_UP) +
							Input.GetActionStrength(InputAxes.ANALOG_UP));

			_InpVecCur.x = Mathf.Clamp(_InpVecCur.x, -1f, 1f);
			_InpVecCur.y = Mathf.Clamp(_InpVecCur.y, -1f, 1f);

			if (_InpVecCur.LengthSquared() > 0)
			{
				_ShouldUpdateMove = true;
			}
			else if (_ShouldUpdateMove)
			{
				_ShouldUpdateMove = false;
				_LastUpdateMove = true;
			}

			if (_InpVecCur.Length() > _Player.AttackRelease.ATTACK_THRESHOLD && 
				_InpVecOld.Length() < _Player.AttackRelease.ATTACK_THRESHOLD)
			{
				_ARCurrentTime = 0f;
				_Releasing = false;
				_Attacking = true;
				_InputVectorGoal = _InpVecCur;
			}
			//>(Blanche) TODO: Make a better detection of input release, currently it's based on the fact that the threshold is low enough to not be perceivable
			else if (_InpVecCur.Length() < _Player.AttackRelease.RELEASE_THRESHOLD && 
					 _InpVecOld.Length() > _Player.AttackRelease.RELEASE_THRESHOLD)
			{
				_ARCurrentTime = 0f;
				_Attacking = false;
				_Releasing = true;
				_InputVectorFinal = _InpVecOld;
			}

			_InpVecOld = _InpVecCur;
		}

        public override void
        _PhysicsProcess(float delta)
		{
			if (_Attacking)
			{
				_ARCurrentTime += delta;
				if (_ARCurrentTime >= _Player.AttackRelease.AttackTime)
				{
					_InpVecCur.x = (Input.GetActionStrength(InputAxes.DIGITAL_RIGHT) + 
									Input.GetActionStrength(InputAxes.ANALOG_RIGHT))
								 - (Input.GetActionStrength(InputAxes.DIGITAL_LEFT) + 
									Input.GetActionStrength(InputAxes.ANALOG_LEFT));
					_InpVecCur.y = (Input.GetActionStrength(InputAxes.DIGITAL_DOWN) + 
									Input.GetActionStrength(InputAxes.ANALOG_DOWN))
								 - (Input.GetActionStrength(InputAxes.DIGITAL_UP) + 
									Input.GetActionStrength(InputAxes.ANALOG_UP));

					_InpVecCur.x = Mathf.Clamp(_InpVecCur.x, -1f, 1f);
					_InpVecCur.y = Mathf.Clamp(_InpVecCur.y, -1f, 1f);

					_Attacking = false;
					_ARCurrentTime = 0f;
				}
				else
				{
					if (_InpVecCur.LengthSquared() > _InputVectorGoal.LengthSquared())
					{
						_InputVectorGoal = _InpVecCur;
					}
					_InpVecCur = _InputVectorGoal * _Player.AttackRelease.Attack.Curve
								 .Interpolate(_ARCurrentTime / _Player.AttackRelease.AttackTime);
				}
			}
			else if (_Releasing)
			{
				_ARCurrentTime += delta;
				_InpVecCur = _InputVectorFinal;
				if (_ARCurrentTime >= _Player.AttackRelease.ReleaseTime)
				{
					_InpVecCur *= 0f;
					_Releasing = false;
					_LastUpdateMove = true;
					_ARCurrentTime = 0f;
				}
				else
				{
					_InpVecCur *= _Player.AttackRelease.Release.Curve
								  .Interpolate(_ARCurrentTime / _Player.AttackRelease.ReleaseTime);
				}
			}

			if (_ShouldUpdateMove || _LastUpdateMove || _Attacking || _Releasing)
			{
				_Player.CurrentController.Move(_InpVecCur.x, _InpVecCur.y);

				if (_LastUpdateMove)
				{
					_LastUpdateMove = false;
				}
			}
		}

		public void
		PutOnHold()
        {
			_Player.CurrentController.Move(0f, 0f);
			_ARCurrentTime = 0f;
			_Attacking = false;
			_Releasing = false;
			_ShouldUpdateMove = false;
			_LastUpdateMove = false;
			_InpVecCur = Vector2.Zero;
			_InpVecOld = Vector2.Zero;
			_InputVectorGoal = Vector2.Zero;
			_InputVectorFinal = Vector2.Zero;
		}
    }
}
