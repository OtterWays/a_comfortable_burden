namespace otter_ways.cm.systems.input
{
    public class PauseMenu : State
    {
        public
        PauseMenu(Player player) : base(player)
        { }

		public override void
		_UnhandledInput(Godot.InputEvent @event)
		{
			if (Godot.Input.IsActionJustPressed(InputAxes.PAUSE_MENU))
			{
				_Player.EmitPressedPause();
				_Player.ToGame();
				return;
			}
		}
	}
}
