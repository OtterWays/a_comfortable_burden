using Godot;

namespace otter_ways.cm.systems
{
	public class MentalLoad : Resource
	{
		[Signal] public delegate void Full();
		[Signal] public delegate void ValueChanged(float newvalue);

		[Export] readonly float _MAXIMUM_LOAD = 100f;
		public float MaximumLoad { get { return _MAXIMUM_LOAD; } }
		[Export] readonly float _DISCOVERY_COST = 3f;
		public float DiscoveryCost { get { return _DISCOVERY_COST; } }
		[Export] readonly float _EXECUTION_COST = 10f;
		public float ExecutionCost { get { return _EXECUTION_COST; } }
		[Export] readonly float _MOVEMENT_COST = 2.2f;
		public float MovementCost { get; private set; }

		float _CurrentLoad;

		public void
		Initialise()
		{
			MovementCost = _MOVEMENT_COST / 60f;
		}

		public void
		IncreaseLoad(float toadd)
		{
			_CurrentLoad += toadd;
			EmitSignal(nameof(ValueChanged), _CurrentLoad);

			if (_CurrentLoad >= _MAXIMUM_LOAD)
			{
				_CurrentLoad = 0f;
				EmitSignal(nameof(Full));
			}
		}

		public void
		Reset()
		{
			_CurrentLoad = 0f;
			EmitSignal(nameof(ValueChanged), _CurrentLoad);
		}
	}
}
