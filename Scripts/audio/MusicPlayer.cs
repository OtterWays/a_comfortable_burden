using Godot;

namespace otter_ways.cm.audio
{
	public class MusicPlayer : AudioStreamPlayer
	{
		[Export] systems.GameRules _GameRules;
		[Export(PropertyHint.Range, "-60,0,-1")] readonly float _LOWERED_VOLUME = -5f;
		[Export] readonly float _LERP_TIME = .2f;

		float _CurrentLerpTime;
		float _CurrentLerpValue;
		bool _Decrease;
		bool _Increase;
		bool _FadeOut;

		public override void
		_EnterTree()
		{
			_ = _GameRules.Connect(nameof(systems.GameRules.LevelStarting), this, nameof(OnGameRulesLevelStarting));
			_ = _GameRules.Connect(nameof(systems.GameRules.Victory), this, nameof(OnGameRulesVictory));
			_ = _GameRules.Connect(nameof(systems.GameRules.Defeat), this, nameof(OnGameRulesDefeat));
			_ = _GameRules.Connect(nameof(systems.GameRules.GameEnd), this, nameof(OnGameRulesVictory));
		}

		public override void 
		_Ready()
		{
			SetProcess(false);
		}

		public override void
		_ExitTree()
		{
			_GameRules.Disconnect(nameof(systems.GameRules.LevelStarting), this, nameof(OnGameRulesLevelStarting));
			_GameRules.Disconnect(nameof(systems.GameRules.Victory), this, nameof(OnGameRulesVictory));
			_GameRules.Disconnect(nameof(systems.GameRules.Defeat), this, nameof(OnGameRulesDefeat));
			_GameRules.Disconnect(nameof(systems.GameRules.GameEnd), this, nameof(OnGameRulesVictory));
		}

		public override void 
		_Process(float delta)
		{
			if (_CurrentLerpTime < _LERP_TIME)
			{
				_CurrentLerpTime += delta;
				_CurrentLerpValue += delta / _LERP_TIME;
				if (_Decrease)
				{
					VolumeDb = _CurrentLerpValue * _LOWERED_VOLUME;
				}
				else if (_FadeOut)
				{
					VolumeDb = _CurrentLerpValue * -80f;
				}
				else
				{ 
					VolumeDb = (1 - _CurrentLerpValue) * _LOWERED_VOLUME;
				}
			}
			else
			{
				_CurrentLerpTime = 0f;
				_CurrentLerpValue = 0f;
				VolumeDb = _Decrease ? _LOWERED_VOLUME : _FadeOut ? -80f : 0f;
				_Decrease = false;
				_Increase = false;
				if (_FadeOut)
				{
					Stop();
				}
				_FadeOut = false;
				SetProcess(false);
			}
		}

		void
		OnGameRulesLevelStarting()
		{
			_FadeOut = false;
			_Increase = true;
			Play();
			SetProcess(true);
		}

		void
		OnGameRulesVictory()
		{
			_Decrease = true;
			SetProcess(true);
		}

		void
		OnGameRulesDefeat()
		{
			_Decrease = true;
			SetProcess(true);
		}

		void
		OnUIRequestSceneChange()
		{
			_Decrease = false;
			_FadeOut = true;
			SetProcess(true);
		}
	}
}
