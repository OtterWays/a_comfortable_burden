using Godot;

namespace otter_ways.cm
{
	public class Settings : Resource
	{
        const string _MUSIC = "Music";
        const string _SFX = "SFX";

		[Signal] public delegate void ColourUpdated();
		[Signal] public delegate void LanguageUpdated();
		[Signal] public delegate void MusicVolumeUpdated();
		[Signal] public delegate void EffectsVolumeUpdated();

		[Export] float _Music;
		public float Music { get { return _Music; } }
		[Export] float _Effects;
		public float Effects { get { return _Effects; } }
		[Export] string _Language;
		public string Language { get { return _Language; } }
		[Export] Color _OutlineColour;
		public Color OutlineColour { get { return _OutlineColour; } }

		public void
		UpdateMusicVolume(float musicvolume)
        {
			if (Mathf.IsZeroApprox(musicvolume))
            {
				musicvolume = 0.0001f;
            }
			_Music = musicvolume;
			AudioServer.SetBusVolumeDb(AudioServer.GetBusIndex(_MUSIC), GD.Linear2Db(musicvolume));
			EmitSignal(nameof(MusicVolumeUpdated));
		}

		public void
		UpdateEffectsVolume(float effectsvolume)
		{
			if (Mathf.IsZeroApprox(effectsvolume))
			{
				effectsvolume = 0.0001f;
			}
			_Effects = effectsvolume;
			AudioServer.SetBusVolumeDb(AudioServer.GetBusIndex(_SFX), GD.Linear2Db(effectsvolume));
			EmitSignal(nameof(EffectsVolumeUpdated));
		}

		public void
		UpdateLanguage(string language)
		{
			_Language = language;
			TranslationServer.SetLocale(language);
			EmitSignal(nameof(LanguageUpdated));
		}

		public void
		UpdateOutlineColour(Color outlinecolour)
		{
			_OutlineColour = outlinecolour;
			EmitSignal(nameof(ColourUpdated));
		}
	}
}
