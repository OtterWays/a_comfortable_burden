using Godot;

namespace otter_ways.cm
{
    public class CameraShakeInfo : Resource
    {
        [Export] float _Intensity = .1f;
        public float Intensity { get { return _Intensity; } }
        [Export] float _Decay = .7f;
        public float Decay { get { return _Decay; } }
        [Export] float _ZAmplitude = 30f;
        public float ZAmplitude { get { return Mathf.Deg2Rad(_ZAmplitude); } }
        [Export] Vector2 _X_Y_Amplitude = new Vector2(45f, 45f);
        public Vector2 XYAmplitude { get { return _X_Y_Amplitude; } }
    }
}