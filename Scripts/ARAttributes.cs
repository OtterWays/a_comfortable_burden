using Godot;

namespace otter_ways.cm
{
	public class ARAttributes : Resource
	{
		[Export] readonly float _ATTACK_THRESHOLD;
		public float ATTACK_THRESHOLD { get { return _ATTACK_THRESHOLD; } }
		[Export] float _AttackTime = .160f;
		public float AttackTime { get { return _AttackTime; } }
		[Export] CurveTexture _Attack;
		public CurveTexture Attack { get { return _Attack; } }
		[Export] readonly float _RELEASE_THRESHOLD;
		public float RELEASE_THRESHOLD { get { return _RELEASE_THRESHOLD; } }
		[Export] float _ReleaseTime = .160f;
		public float ReleaseTime { get { return _ReleaseTime; } }
		[Export] CurveTexture _Release;
		public CurveTexture Release { get { return _Release; } }
	}
}
