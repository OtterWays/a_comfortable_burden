using Godot;

namespace otter_ways.cm
{
	public class CameraRig : Node2D
	{
		[Export] systems.Player _Player;
		[Export] NodePath _Camera2DPath;
		[Export] CameraShakeInfo _ShakeInfoDefault;

		Camera2D _Camera2D;
		actors.playable.Controller _Target;

		RandomNumberGenerator _Random;
		OpenSimplexNoise _Simplex;
		CameraShakeInfo _ShakeInfoCurrent;
		
		float _ShakeSum; //[0, _ShakeSum, 1]
		float _ShakeSumSqrd;
		int _ShakeY;

		public override void
		_EnterTree()
		{
			_Random = new RandomNumberGenerator();
			_Random.Randomize();
			_Simplex = new OpenSimplexNoise();
			_Simplex.Seed = (int)_Random.Randi();
			_Simplex.Octaves = 2;
			_Simplex.Period = 4;
			_Camera2D = GetNode<Camera2D>(_Camera2DPath);
			_ = _Player.Connect(nameof(systems.Player.ControllerChanged), this, nameof(OnPlayerControllerChanged));
		}

		public override void 
		_ExitTree()
		{
			_Player.Disconnect(nameof(systems.Player.ControllerChanged), this, nameof(OnPlayerControllerChanged));
		}

		public override void
		_Ready()
		{
			SetPhysicsProcess(false);
			SetProcess(false);
		}

		public override void
		_PhysicsProcess(float delta)
		{
			Position = _Target.Position;
		}

		public override void
		_Process(float delta)
		{
			//TODO(Blanche): Bring back gradually to the previous camera offset and rotation rather than 0
			if (_ShakeSum > 0f)
			{
				_ShakeSum = Mathf.Max(_ShakeSum - _ShakeInfoCurrent.Decay * delta, 0f);
				_ShakeSumSqrd = _ShakeSum * _ShakeSum;
				_ShakeY++;
				_Camera2D.Rotation = _ShakeInfoCurrent.ZAmplitude * _ShakeSumSqrd * _Simplex.GetNoise2d(_Simplex.Seed, _ShakeY);
				_Camera2D.Offset = new Vector2(
					_ShakeInfoCurrent.XYAmplitude.x * _ShakeSumSqrd * _Simplex.GetNoise2d(_Simplex.Seed * 2, _ShakeY),
					_ShakeInfoCurrent.XYAmplitude.y * _ShakeSumSqrd * _Simplex.GetNoise2d(_Simplex.Seed * 3, _ShakeY)
				);
			}
			else
			{
				SetProcess(false);
			}
		}

		void
		OnPlayerControllerChanged()
		{
			_Target = _Player.CurrentController;
			_Camera2D.Zoom = _Player.CurrentController.CameraParameters;
			SetPhysicsProcess(true);
		}

		public void
		Shake(CameraShakeInfo info = null)
		{
			_ShakeInfoCurrent = info != null ? info : _ShakeInfoDefault;

			_ShakeSum = Mathf.Min(_ShakeSum + _ShakeInfoCurrent.Intensity, 1f);
			SetProcess(true);
		}

		public void
		Reboot()
		{
			//TODO(Blanche): Reset to previous offset, rotation rather than default
			_ShakeSum = 0f;
			_ShakeInfoCurrent = null;
			_Camera2D.Rotation = 0f;
			_Camera2D.Offset = Vector2.Zero;
		}

		public void
		SetTarget(actors.playable.Controller target)
		{
			_Target = target;
			_Camera2D.Zoom = target.CameraParameters;
		}
	}
}
